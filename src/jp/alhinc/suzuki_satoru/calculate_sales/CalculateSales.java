package jp.alhinc.suzuki_satoru.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//エラー処理3-1 コマンドライン引数の確認
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>(); //支店定義ファイル
		Map<String, Long> branchSales = new HashMap<>();  //売上ファイル
		Map<String, String>commodityNames = new HashMap<>(); //商品定義ファイル
		Map<String, Long>commoditySales = new HashMap<>(); //商品売上ファイル


		// 支店定義ファイル、商品定義ファイル読み込み

		if(!input(args[0], "branch.lst", "支店", "[0-9]{3}", branchNames, branchSales)) {
			return;
		}

		if(!input(args[0], "commodity.lst", "商品", "[A-Za-z0-9]{8}", commodityNames, commoditySales)) {
			return;
		}


		//集計

		//フォルダ内ファイルをすべて取得
		File[] files = new File(args[0]).listFiles();
		//8桁.rcd判別trueを格納するlist作成
		List<File> rcdFiles = new ArrayList<File>();


		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();

			//ファイルの確認 & 8桁.rcdの判別
			if(files[i].isFile() && fileName.matches("[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);

		//エラー処理3-1 連番確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int formar = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - formar) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

		}

		//売上ファイルrcdFiles読み込み
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				//List rcdfiles読み込み
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				ArrayList<String> rcdList = new ArrayList<String>();

				while((line = br.readLine()) != null ) {
					rcdList.add(line);
				}

				//エラー処理3-5 売上ファイルフォーマット確認
				if(rcdList.size() != 3) {
					System.out.println(rcdFiles.get(i) + "のフォーマットが不正です");
					return;
				}

				//エラー処理3-3 支店コード比較
				if(!branchNames.containsKey(rcdList.get(0))) {
					System.out.println(rcdFiles.get(i) + "の支店コードが不正です");
					return;
				}
				//エラー処理3-4
				if(!commodityNames.containsKey(rcdList.get(1))) {
					System.out.println(rcdFiles.get(i) + "の商品コードが不正です");
					return;
				}
				//エラー処理3-2 売上ファイル金額の数値確認
				if(!rcdList.get(2).matches("[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}

				//longに変換
				long fileSales = Long.parseLong(rcdList.get(2));
				//売上map + fileSales
				long saleAmount = branchSales.get(rcdList.get(0)) + fileSales;
				//System.out.println(saleAmount);

				long comSaleAmount = commoditySales.get(rcdList.get(1)) + fileSales;


				//エラー処理3-2 売上金額合計10桁越え確認
				if(saleAmount >= 10000000000L || comSaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//売上追加したsaleAmount,comSaleAmountを売上mapに再put
				branchSales.put(rcdList.get(0), saleAmount);
				commoditySales.put(rcdList.get(1), comSaleAmount);


			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;

			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}


		}

		//ファイル出力メソッド呼び出し
		if(!output(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		if(!output(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}

	}




	//ファイル読み込みメソッド
	public static boolean input(String path, String inFileName, String errFileName, String fileFormat, Map<String, String> mapNames, Map<String, Long> mapSales) {

		BufferedReader br = null;

		try {
			File file = new File(path, inFileName);
			//エラー処理1-1 支店定義ファイル確認
			if(!file.exists()) {
				System.out.println( errFileName +"定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				System.out.println(line);
				String[] items = line.split(","); // 支店コード, 支店名


				//エラー処理1-2 支店定義ファイルフォーマット確認
				if((items.length != 2 || (!items[0].matches(fileFormat))) ) {
					System.out.println( errFileName + "定義ファイルのフォーマットが不正です");
					return false;
				}

				mapNames.put(items[0], items[1]); //key:支店コード, value: 支店名
				mapSales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;

		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//ファイル出力メソッド
	public static boolean output(String path, String outFileName, Map<String,String> mapNames, Map<String, Long> mapSales) {
		BufferedWriter bw = null;
		try {
			File outFile = new File(path, outFileName);
			FileWriter fw = new FileWriter(outFile);
			bw = new BufferedWriter(fw);

			for(String key : mapNames.keySet()) {
				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



}
